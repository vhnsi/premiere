# Les labyrinthes

## Génération de labyrinthes

![Génération de labyrinthes par fusion](animation_fusion.gif)

Télécharger le fichier `activite_eleve.py` dans le répertoire Generation.

### Algorithme de la fusion aléatoire
	
L'algorithme associe une valeur unique à chacune des cellules que l'on appellera identifiant. On commence l'algorithme avec tous les murs internes fermés.

À chaque itération, on choisit un mur aléatoirement, c'est le mur que nous allons ouvrir afin de créer un chemin entre deux ou plusieurs cellules adjacentes.

Avant de pouvoir ouvrir le mur, il faut vérifier si les identifiants des cellules sont différents.

1. Si les identifiants sont identiques, celà signifie que les deux cellules reliées et appartiennent au même chemin. Et par conséquent on ne peut ouvrir le mur.
2. Sinon, les identifiants sont différents, alors on ouvre le mur et l'on affecte l'identifiant de la première cellule à toutes les cellules du second chemin.


## Résolution de labyrinthes

Télécharger l'intégralité du dossier Resolution.

Ouvrir ensuite sur Mozilla Firefox la page html se trouvant dans le dossier html.

Pour chaque algorithme et chaque labyrinthe résolver les labyrinthes et noter sur une feuille le nombre de cases parcourues.


## Liens

[Questionnaire](https://forms.gle/3wfhyBhGYR8Z6sjT7)
