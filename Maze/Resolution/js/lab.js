let cell_actuel;
let chemin = [];
let cell_arr_l1;
let cell_arr_l2;
let cell_arr_l3;
const bougie_cell = new Set();
let cell_dep;
let cell_prec;
let id_lab;
let methode;
/* idée

Rajouteur un compteur sur le nombre de click, qui permettra de voir le nombre de cases exploréer en plus du chemin

Besoin: Importer un fichier js dans un autre fichier js ?
- Encart pour la Méthode
- encart pour calcul ( cf pledge)
- variable pour le chemin (nb de cases parcouru)
- Plusieurs labyrinthe différents

=> Complexité: longueur du chemin

Interface 1 : Debut -> choix de la méthode de résolution -> display none
-> menu nav
-> differents lab
Interface 2: Acces au labyrinthe
Interface 3: Victoire : Affiche le affiche_chemin
Interface 4: Redémarrer le même labyrinthe avec la même méthode \
 sinon menu principal (Interface 1)

Méthode de résolution:
- exhaustive
- trémaux
- pledge
- bougie (hasard)
- main droite
- main gauche
*/

//Longueur du chemins
const longueur_chemin = () => {
  let taille = chemin.length;
  const lg_chemin = document.getElementById('lg_chemin');
  lg_chemin.innerHTML = "</br></br>Le nombre de cases parcourues est :  " + taille;
}

// Affiche lab
const affiche_lab = (name) => {
  let lab1 = document.getElementById('lab1');
  let lab2 = document.getElementById('lab2');
  let lab3 = document.getElementById('lab3');
  switch (name) {
    case "lab1":
      lab1.style.display= 'inherit';
      lab2.style.display = 'none';
      lab3.style.display = 'none';
      id_lab = 'lab1';
      break;
    case 'lab2':
      lab2.style.display= 'inherit';
      lab1.style.display = 'none';
      lab3.style.display = 'none';
      id_lab = 'lab2';
      break;
    case 'lab3':
      lab3.style.display= 'inherit';
      lab2.style.display = 'none';
      lab1.style.display = 'none';
      id_lab = 'lab3';
      break;
  }
}

/*
const tremaux = () => {
  const lab = document.getElementById("buttons");
  b1 = document.createElement('button');
  b1.id = "tremaux";
  b1.textContent = "Tremaux";
  //b1.addEventListener("click",pledge);
  lab.appendChild(b1);
}*/

//pledge
const ajoute_un = () => {
  const encart = document.getElementById("encart");
  somme = parseInt(encart.innerHTML);
  encart.innerHTML = somme +1;
}

const retire_un = () => {
  const encart = document.getElementById("encart");
  somme = parseInt(encart.innerHTML);
  encart.innerHTML = somme -1;
}

const remet_a_zero = () => {
  const encart = document.getElementById("encart");
  somme = parseInt(encart.innerHTML);
  encart.innerHTML = 0;
}
/*const prepare_pledge = () => {
  const pl_b = document.getElementById("pledge");
  pl_b.addEventListener("click",pledge);
}*/

const pledge = () => {
  const encart = document.getElementById("b_util");
  b1 = document.createElement('button');
  b1.id = "plus1";
  b1.textContent = "Ajoute un";
  b1.addEventListener("click",ajoute_un);
  b2 = document.createElement('button');
  b2.id = "plus1";
  b2.textContent = "Retire un";
  b2.addEventListener("click",retire_un);
  b3 = document.createElement('button');
  b3.id = "zero";
  b3.textContent = "Remet à zéro";
  b3.addEventListener("click",remet_a_zero);
  encart.appendChild(b1);
  encart.appendChild(b2);
  encart.appendChild(b3);
  encart.style.display = "none";
  //encart();
}

const init_event_button_lab = () => {
  const lab_1 = document.getElementById('lab_1');
  lab_1.addEventListener("click",fct => affiche_lab('lab1'));
  const lab_2 = document.getElementById('lab_2');
  lab_2.addEventListener("click",fct => affiche_lab('lab2'));
  const lab_3 = document.getElementById('lab_3');
  lab_3.addEventListener("click",fct => affiche_lab('lab3'));
}

const init = () => {
  //ajout_div();
  //nav();
  ajoute_bouttons();
  pledge();
  dir_alea();
  init_event_button_lab();
  methode_res_aff();
  const liste_cellules = document.querySelectorAll('td');
  for (let cell of liste_cellules){
    cell.visited = false;
    cell.style.borderColor = 'white';
    cell.x = parseInt(cell.id[7]);
    cell.y = parseInt(cell.id[9]);
  }
  let x = 9;
  let y = 9;
  associemur();
  id_lab = 'lab1';
  //affiche_voisin(x,y);
  let id = x + '.' + y;
  cell_dep = document.getElementById(id_lab+'_c_'+id);
  cell_actuel = cell_dep;
  cell_prec = cell_dep;
  cell_arr_l1 = document.getElementById('lab1_c_0.0');
  cell_arr_l2 = document.getElementById('lab2_c_0.0');
  cell_arr_l3 = document.getElementById('lab3_c_0.0');
  //cell_dep.style.backgroundColor = "red";
  //cell_dep.style.borderColor = "black";
  //affiche_cellule(x,y);
  document.addEventListener("keyup",deplace);
}

const methode_res_aff = () => {
  let alea = document.getElementById('alea');
  let md = document.getElementById('main_droite');
  let mg = document.getElementById('main_gauche');
  let pl = document.getElementById('pledge');
  let tr = document.getElementById('tremaux');
  tr.style.display = "none";
  alea.addEventListener("click",fct => {methode = "alea"; methode_res();});
  md.addEventListener("click",fct => {methode = "main_droite"; methode_res();});
  mg.addEventListener("click",fct => {methode = "main_gauche"; methode_res();});
  pl.addEventListener("click",fct => {methode = "pledge"; methode_res();});
  //tr.addEventListener("click",fct => {methode = "tremaux"; methode_res();});
}

const genere_dir_alea = () => {
  let alea =  Math.floor(Math.random() * 4);
  const emp = document.getElementById('dir_alea');
  res = "Direction aléatoire : ";
  switch (alea) {
    case 0:
        res += "Nord";
        emp.textContent = res;
      break;
    case 1:
        res += "Sud";
        emp.textContent = res;
      break;
    case 2:
        res += "Est";
        emp.textContent = res;
      break;
    case 3:
        res += "Ouest";
        emp.textContent = res;
      break;
  }
}

const dir_alea = () => {
  const encart = document.getElementById("b_alea");
  b1 = document.createElement('button');
  b1.id = "alea_dir_b";
  b1.textContent = "Direction aléatoire";
  b2 = document.createElement('div');
  b2.id = "dir_alea";
  b1.addEventListener("click",genere_dir_alea);
  encart.appendChild(b1);
  encart.appendChild(b2);
  encart.style.display = "none";
  rose_des_vents();
}

const methode_res = () => {
  const somme = document.getElementById('encart');
  const encart = document.getElementById("b_util");
  const aleatoire = document.getElementById("b_alea");
  switch (methode) {
    case "alea":
      somme.style.display = "none";
      encart.style.display = "none";
      aleatoire.style.display = "inherit";
      nom_methode = "Aleatoire"
      txt = "Choissiser une direction aléatoire parmis\
      celle qui sont accessibles </br>";
      break;
    case "main_droite":
      somme.style.display = "none";
      encart.style.display = "none";
      aleatoire.style.display = "none";
      nom_methode = "Main droite"
      txt = "Longer systématiquement le mur à votre droite </br>";
      break;
    case "main_gauche":
      somme.style.display = "none";
      encart.style.display = "none";
      aleatoire.style.display = "none";
      nom_methode = "Main gauche"
      txt = "Longer systématiquement le mur à votre gauche </br>";
      break;
    case "pledge":
      encart.style.display = "inherit";
      somme.style.display = "inherit";
      aleatoire.style.display = "none";
      nom_methode = "Pledge"
      txt = "on utilise un compteur initialiser à 0. </br></br>\
      Dès que l'on tournera à droite au réduira ce compteur d'un point,\
       et lorsque nous tournerons à gauche on incrémentera le compteur d'un point.\
      </br></br>\
       L'algorithme est composé de deux étapes:\
       </br>1. On va tout droit jusqu'au mur puis on passe à l'instruction suivante.\
       </br>2. On longe le mur par la droite\
        jusqu'à ce que le compteur atteigne 0.\
        </br>Dès que le compteur atteint 0, on repasse à l'étape 1.</br></br>";
      break;
    case "tremaux":
      somme.style.display = "none";
      encart.style.display = "none";
      aleatoire.style.display = "none";
      nom_methode = "Tremaux"
      txt = "Pour cet algorithme, on a besoin de \
      trois symboles que j'ai préalablement choisi:{. x _}.\
        À chaque jonction (qui est un croisement entre deux chemins),\
        les chemins qui se croisent sont alors soit non marqués ( _ ),\
         soit marqués une fois ( . ) ou alors deux fois ( x ).\
        L'algorithme fonctionne selon les étapes suivantes:\
        * On marque les chemins que l'on parcourt une fois ( . ).\
         Les marques sont visibles sur les deux chemins;\
         *  On n'entre jamais dans un chemin déjà marqué;\
         *  Si on arrive à une jonction qui ne possède pas de marque alors\
          on choisis aléatoirement un chemin non marqué, puis on le marque et on le suit;\
         *  Sinon la jonction a une marque;\
         *  Si le chemin par lequel on est arrivé n'a qu'une marque,\
          alors on fait demi-tour et on le marque une seconde fois (on change son symbole en ( x )).\
           On atteint ce cas-ci lorsque l'on arrive dans un cul-de-sac,\
         *  Sinon on choisit un des chemins possédant le moins de marques possibles,\
          on prend alors ce chemin et on le marque;";
      break;
    }
    //const div_menu_droite = document.getElementById('menu_droite');
    const title = document.getElementById('title_methode');
    const div_methode = document.getElementById('methode');
    div_methode.innerHTML = txt;
    title.textContent = nom_methode;
}

const boutton_input = (type,id,name,value) => {
  const button = document.createElement("input");
  button.type = type;
  button.id = id;
  button.name = name;
  button.value = value;
  return button
}

const label_button = (label,for_att) => {
  let label_b = document.createElement("label");
  label_b.for = for_att;
  label_b.textContent = label;
  return label_b;
}

const a_gagne = () => {
  return cell_actuel == cell_arr_l1 || cell_actuel == cell_arr_l2 || cell_actuel == cell_arr_l3;
}

const victoire = () => {
  if (a_gagne()){
    alert("victoire");
    affiche_chemin();
  }
}
// affichage image
const affiche_image = (parent,dir) => {
  if (! parent.hasChildNodes()){
    let noeud = document.createElement('img');
    noeud.src = "../js/images/fleche2.png";
    noeud.id = "image";
    noeud.alt = "Une image";
    noeud.width = "18";
    noeud.height = "18";
    noeud.style.transform = "rotate("+dir+"turn)";
    //let parent = document.getElementById(id_parent);
    parent.appendChild(noeud);
  }
}

const rose_des_vents = () => {
  const parent = document.getElementById("b_alea");
  let noeud = document.createElement('img');
  noeud.src = "../js/images/Rose_des_vents.png";
  noeud.alt = "Une image";
//  noeud.width = "18";
//  noeud.height = "18";
  //let parent = document.getElementById(id_parent);
  parent.appendChild(noeud);
}

// affichage du chemin
const affiche_chemin = () => {
  longueur_chemin();
  console.log("affiche_chemin");
  const liste_cellules = document.getElementById(id_lab).querySelectorAll('td');
  for (let cell of liste_cellules){
    if (cell.visited){
      cell.style.backgroundColor = "purple";
      cell.style.borderColor = "black";
      chemin.push(cell);
    }else{
      cell.style.backgroundColor = "white";
      cell.style.borderColor = "white";
      //cell.removeEventListener("click", test => affiche_cellule(cell.x,cell.y,false));
      cell.onclick="";
    }
  }
}

//affichage de la cellule
const affiche_cellule = (x ,y,uniq=false, dir) => {
  const liste_cellules = document.getElementById(id_lab).querySelectorAll('td');
//  console.log(x,y);
//  console.log(id_lab+'_c_'+x+'.'+ y);
  let cell = document.getElementById(id_lab+'_c_'+x+'.'+ y);
  cell.style.borderColor = "black";
  cell.visited = true;
  cell_prec = cell_actuel;
  cell_actuel = cell;
  chemin.push(cell_actuel);
  //cell_actuel.innerHTML = "*";

  affiche_image(cell_actuel,dir);
  if (uniq)
    affiche_uniquement_voisin(x,y);
  else
    affiche_voisin(x,y);
  victoire();
}

//affichage des voisins
const affiche_voisin = (x,y) => {
  const voisins = voisin(x,y);
  const liste_cellules = document.getElementById(id_lab).querySelectorAll('td');
  let res = [];
  for (let v of voisins){
    let cell = document.getElementById(id_lab+'_c_'+v.x.toString()+'.'+ v.y.toString());
    cell.style.backgroundColor = 'red';
    //cell.style.borderColor = 'red';
    if (cell != cell_actuel){
      cell.textContent = '';}
    res.push(cell);
    if (!cell.visited){
      cell.style.borderColor = "orange";
      cell.style.backgroundColor = 'orange';

      //cell.addEventListener("click",test => affiche_cellule(v.x,v.y,false));
      cell.onclick = function(){affiche_cellule(v.x,v.y,false)};
    }
    }
    for (let cell of liste_cellules){ //event et couleur
      if (res.includes(cell)){
        cell.style.borderColor = "coral";
        cell.style.backgroundColor = 'coral';
        cell.onclick = function(){affiche_cellule(cell.x,cell.y,false)};
        if (cell == cell_actuel) cell.style.borderColor = "black";
      }
      else{
        cell.style.borderColor = "white";
        cell.style.backgroundColor = 'white';
        cell.onclick = "";
      }
    }
  }

const affiche_uniquement_voisin = (x,y) => {
  const voisins = voisin(x,y);
  const liste_cellules = document.getElementById(id_lab).querySelectorAll('td');
  let res = [];
  for (let v of voisins){
    cell = document.getElementById(id_lab+'_c_'+v.x+'.'+v.y);
    if (cell != cell_actuel){ cell.textContent = '';} //retire l'image
    for (let cell_l of liste_cellules){
      if (cell.x == cell_l.x && cell.y == cell_l.y){
        res.push(cell);
        if (bougie_cell.has(cell)){
        }else  bougie_cell.add(cell);
      }
    }
  }
  for (let cell of liste_cellules){ //event et couleur
    if (res.includes(cell)){
      cell.style.borderColor = "blue";
      cell.style.backgroundColor = 'blue';
      cell.onclick = function(){affiche_cellule(cell.x,cell.y,true)};
      if (cell == cell_actuel) cell.style.borderColor = "black";
    }
    else{
      cell.style.borderColor = "white";
      cell.style.backgroundColor = 'white';
      cell.onclick = "";
    }
  }
}

//voisin
const voisin = (x,y) => {
  const cell = document.getElementById(id_lab+'_c_'+x.toString()+'.'+ y.toString());
  const res = []; //-> [(8,9),(9,8)]
  if (!cell.right){ res.push({x: x,y: y+1})}
  if (!cell.left){ res.push({x: x,y: y-1})}
  if (!cell.top){ res.push({x: x-1,y: y})}
  if (!cell.bottom){ res.push({x: x+1,y: y})}
  res.push({x:x,y:y});
  return res;
}
//mur
const associemur = () => {
  const liste_cellules = document.querySelectorAll('td');
  const mur = "3px solid white"
  for (let cell of liste_cellules){
    /*cell.right = false;
    cell.left = false;
    cell.top = false;
    cell.bottom = false;*/
    cell.right = cell.style.borderRight == mur;
    cell.left = cell.style.borderLeft == mur;
    cell.top = cell.style.borderTop == mur;
    cell.bottom = cell.style.borderBottom == mur;
  }
}

//bouttons
const ajoute_bouttons = () => {
  const lab = document.getElementById("buttons");
  boutton1 = document.createElement('button');
  boutton1.id = "v1";
  boutton1.textContent = "Start";
  boutton1.addEventListener("click", voisin => double_click_start(9,9));
  lab.appendChild(boutton1);
  boutton2 = document.createElement('button');
  boutton2.id = "v2";
  boutton2.textContent = "affiche_chemin";
  boutton2.addEventListener("click",affiche_chemin);
  lab.appendChild(boutton2);
  /*bougie = document.createElement('button');
  bougie.id = "bougie";
  bougie.textContent = "bougie";
  lab.appendChild(bougie);
  bougie.addEventListener("click",uniq_voisin => affiche_uniquement_voisin(9,9));*/
  b_restart = document.createElement("button");
  b_restart.id = "restart";
  b_restart.textContent = "Restart";
  b_restart.addEventListener("click",restart);
  lab.appendChild(b_restart);
}

const double_click_start = (x,y) => {
  restart();
  affiche_cellule(x,y);
}

const restart = () => {
  const liste_cellules = document.querySelectorAll('td');
  for (let cell of liste_cellules){
    cell.style.borderColor = 'white';
    cell.style.backgroundColor = "white";
    cell.textContent = "";
    cell.visited = false;
  }
  cell_actuel = cell_dep;
  chemin = [];
  remet_a_zero();
  const lg_chemin = document.getElementById('lg_chemin');
  lg_chemin.textContent = "";
}

// deplace clavier
const deplace = event => {
  let x = cell_actuel.x;
  let y = cell_actuel.y;
  let image = document.getElementById("image");
  switch (event.key) {
    case "ArrowDown":
      if (!cell_actuel.bottom){
        x++;
        affiche_cellule(x,y,false,0.25);
      }
      break;
    case "ArrowUp":
      if (!cell_actuel.top){
        x--;
        affiche_cellule(x,y,false,0.75);
      }
      break;
    case "ArrowLeft":
      if (!cell_actuel.left){
        y--;
        affiche_cellule(x,y,false,0.5);
      }
      break;
    case "ArrowRight":
      if (!cell_actuel.right){
        y++;
        affiche_cellule(x,y,false,0);
      }
      break;
  }
}
window.addEventListener('DOMContentLoaded', init);
