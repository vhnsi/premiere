from random import randint,choice
from math import floor
from PIL import Image, ImageDraw

LIGNE = 10
COLONNE = 10
cells = [[{'N':False, 'E':False, 'S':False, 'O': False} for _ in range(LIGNE)] for _ in range(COLONNE)]

def initilaise_zone():
    """
    initialise les zones des celulles
    """
    #initialisation des zones
    pass

def cellule_voisine(i,j):
    """
    Renvoie l'ensemble des directions possible pour une cellule
    :Exemple:
    >>> cellule_voisine(0,0)
    {'S','E'}
    >>> cellule_voisine(1,1)
    {'N','S','O','E'}
    """
    # 9 cas a gerer
    pass

def allZoneAzero():
    """
    Vérifie si toutes les cellules appartiennent à la même zone (càd. à la zone zéro).
    Renvoie: True si toutes les cellules appartiennent à la même zone, False sinon.
    """
    pass


def metZone(zoneInitial, zoneFinal):
    """
    Cette méthode est appelé par la méthode fusionner pour fusionner deux zones.
    Prend en entrée les numéros de deux zone et
    définit la zone de tous les cellules de la grille
    appartenant à l'une de ces deux zones au minimun des deux zones
    """
    pass


def fusionner(i,j,dir):
    """
    Fusionne deux cellules voisines dans la même zone.
    Args:
    - i (int): La coordonnée y de la cellule dans la grille.
    - j (int): La coordonnée x de la cellule dans la grille.
    - dir (str): La direction selon laquelle la cellule doit être fusionnée (N,S,E ou O).
    """
    pass

def fusion():
    """
    Fonction principale
    Initialise les zones
    Tant que toutes les zones ne sont pas à zéros,
    on tire aléatoirement une case (x,y) ainsi qu'une direction
    et on fusionne les cellules selon la méthode fusionner
    """
    pass
    


### Ne pas modifier

def affiche(print_zones=False):
    #alias :
    w=LIGNE;h=COLONNE;c=cells;
    #si on imprime les zones, il faut élargir la taille des couloirs
    if(print_zones):
        len_zone=max([ max([ len(str(cells[i][j]['zone'])) for i in range(h) ]) for j in range(w) ])+1
    inters=[' ','╴','╷', '┐','╶','─','┌','┬','╵','┘','│','┤','└','┴','├','┼']
    t=""
    #la grille des intersections de cases est de taille (N+1)(M+1)
    for i in range(h+1):
        interligne=""
        for j in range(w+1):
            #up, right, bottom, left : les 4 parties de la croix "┼" #False = mur, True = pas mur
            #Coins et bords:
            up=False if i==0 else None
            left=False if j==0 else None
            right=False if j==w else None
            bottom=False if i==h else None
            if j==w:
                if up==None:up=not c[i-1][j-1]['E']
                if bottom==None:bottom=not c[i][j-1]['E']
            if i==h:
                bottom=False
                if right==None:right=not c[i-1][j]['S']
                if left==None:left=not c[i-1][j-1]['S']
            #intérieur :
            if up==None:up=not c[i-1][j]['O']
            if right==None:right=not c[i][j]['N']
            if bottom==None:bottom=not c[i][j]['O']
            if left==None:left=not c[i][j-1]['N']
            #-> mot binaire à 4 bits. 16 cas qu'on a mis dans l'ordre dans la liste inters
            #indice inters
            k=-up*8+right*4+bottom*2+left
            if not print_zones:
                #espacement horizontal supplémentaire
                sep= "─" if left else " "
                t+=sep+inters[k]
                if j==w:t+="\n"
            else:
                sep= (len_zone+2)*"─" if right else (len_zone+2)*" "
                num_zone=cells[i][j]["zone"] if i<h and j<w else ""
                len_sp_left=floor((len_zone - len(str(num_zone)))/2)
                len_sp_right=len_zone-len(str(num_zone))-len_sp_left
                txt_num_zone=str(num_zone)
                interligne+=("│" if bottom else " ")+" "*(len_sp_left+1)+txt_num_zone+" "*(len_sp_right+1)
                t+=inters[k]+sep
                if j==w:
                    t+="\n" + interligne + "\n"
    print(t)

def to_png(filename,save=True,gif = False,cell_size=20):
    image_width = cell_size * LIGNE
    image_height = cell_size * COLONNE
    background = (255,255,255)
    wall = (0,0,0)
    
    image = Image.new("RGBA",(image_width+1,image_height+1),background)
    draw = ImageDraw.Draw(image)
    
    modes = ["BACKGROUND","WALLS"]
    for mode in modes:
        for col in range(COLONNE):
            for ligne in range(LIGNE):
                cell = cells[ligne][col]
                x1 = col * cell_size
                y1 = ligne * cell_size
                x2 = (col + 1) * cell_size
                y2 = (ligne + 1) * cell_size
                
                if mode == "BACKGROUND":
                    color = (255,255,255)
                    draw.rectangle((x1,y1,x2,y2),fill = color)
                else:
                    number = cell['zone']
                    x = col * cell_size + 5
                    y = ligne * cell_size + 5
                    draw.text((x, y), str(number), fill=(0, 0, 0),align="center", anchor="mb")
                    if not cell['N']: #not cell.north:
                        draw.line((x1,y1,x2,y1),wall)
                    if not cell['O']: #not cell.west:
                        draw.line((x1,y1,x1,y2),fill = wall, width = 1)
                    if not cell['E']: #not cell.is_linked(cell.east):
                        draw.line((x2,y1,x2,y2),fill = wall, width = 1)
                    if not cell['S']: #not cell.is_linked(cell.south):
                        draw.line((x1,y2,x2,y2),fill = wall, width = 1)
    if save:
        image.save("{}.png".format(filename),"PNG", optimize=True)
    if gif:
        images.append(image)
        
        
images = []

def gif():
    # Crée le GIF en enregistrant chaque image en tant que "frame"
    images[0].save('animation_fusion.gif', save_all=True, append_images=images[1:], duration=500, loop=0)
