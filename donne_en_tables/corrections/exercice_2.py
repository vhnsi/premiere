import csv
### Exercice 2

## Question 1

def lecture(nom_fichier:str) -> list[dict]:        
    listePop = []
    with open(nom_fichier, 'r', encoding='utf-8') as objFichier:
        objDatasCsv = csv.DictReader(objFichier, delimiter=';')
        for ligne in objDatasCsv:
            listePop.append(dict(ligne))
    return listePop

table = lecture("../src/pokemon.csv")

## Question 2

def attributs(liste_dico: list[dict]) -> list:
    res = []
    for cle in liste_dico[0].keys():
        res.append(cle)
    return res

## Question 3

def attack_sup(liste_dico: list[dict],n) -> list:
    res = []
    for poke in liste_dico:
        if poke['Attack'] > n:
            res.append(poke['Name'])
    return res

## Question 4

def doublons(liste_dico: list[dict]) -> list:
    res = []
    for poke in liste_dico:
        nom = poke['Name']
        if nom not in res:
            res.append(nom)
    return res

## Question 5

def moyenne(liste_dico: list[dict],n) -> list:
    nombre_poke = 0
    somme = 0
    for poke in liste_dico:
        if int(poke['HP']) < n:
            nombre_poke += 1
            somme += int(poke['Attack'])
            print(poke['Attack'],poke['Name'])
    return somme / nombre_poke
    