import csv
### Exercice 3

# 1. ';'
# 2. A cOMPLETER

# 3. Quel est le nombre de communes en France d'après ce fichier ?
# - 39201
# 4. Quelles sont les coordonnées GPS de la ville d'Haubourdin ?
# - 50.6044, 2.9872
# 5. Quel est le nombre de communes dans le département 59 ?
# - 683
# 6. Quelle est la ville de plus haute latitude ? de plus basse longitude ?
# - Bray-Dunes
# - Aigre
# 7. Combien de communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M ?
# -  276
# 8. Quelles sont les 5 communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M *et* qui ont la plus grande latitude ?
# - Varouville
# - Urville-Nacqueville
# - Valcanville
# - Tonneville
# - Vauville

## Question 1

def lecture(nom_fichier:str) -> list[dict]:        
    listePop = []
    with open(nom_fichier, 'r', encoding='utf-8') as objFichier:
        objDatasCsv = csv.DictReader(objFichier, delimiter=';')
        for ligne in objDatasCsv:
            listePop.append(dict(ligne))
    return listePop

table = lecture("../src/pokemon.csv")

## Question 2