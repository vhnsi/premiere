import csv

### Application 1

def lecture(nom_fichier:str) -> list[dict]:        
    listePop = []
    with open(nom_fichier, 'r', encoding='utf-8') as objFichier:
        objDatasCsv = csv.DictReader(objFichier, delimiter=';')
        for ligne in objDatasCsv:
            listePop.append(dict(ligne))
    return listePop

table = lecture("../src/vlille-realtime2.csv")

### Application 2

def nom_stations_boucle(liste_dict : list[dict] )-> list:
    res = []
    for station in liste_dict:
        res.append(station['nom'])
    return res

def nom_stations_comprehension(liste_dict : list[dict] )-> list:
    return [station['nom'] for station in liste_dict]

### Application 3

def nom_stations_hs_boucle(liste_dict : list[dict]) -> list:
    res = []
    for station in liste_dict:
        if station['etat'] != 'EN SERVICE':
            res.append(station['nom'])
    return res

def nom_stations_hs_compréhension(liste_dict : list[dict] )-> list:
    return [station['nom'] for station in liste_dict if station["etat"] != 'EN SERVICE']

### Application 4
    
def nom_station_max_vlille(liste_dict : list[dict]) -> str:
    max = liste_dict[0]
    for station in liste_dict:
        if station["nbPlacesDispo"] > max["nbPlacesDispo"]:
            max = station
    return station['nom']

### Application 5

def fonc_cle_de_tri(station):
    return int(station["nbVelosDispo"])

def nom_station_max_velos_dispo(nom_fichier : str) -> list:
    table = lecture(nom_fichier)
    table.sort(key=fonc_cle_de_tri)
    res = [table[-1]['nom'],table[-2]['nom'],table[-3]['nom']]
    return res

### Application 6

dep = lecture("../src/departements.csv")
pop = lecture("../src/population_hdf.csv")

def jointure(table1, table2,cle1,cle2):
    table_fusion = []
    for elt_t1 in table1:
        for elt_t2 in table2:
            if elt_t1[cle1] == elt_t2[cle2]:
                new_dico = dict(elt_t1)
                for key in elt_t2:
                    if key != cle2:
                        new_dico[key] = elt_t2[key]
                table_fusion.append(new_dico)
    return table_fusion
                
                