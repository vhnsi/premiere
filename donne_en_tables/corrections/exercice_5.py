import csv
### Exercice 5

## Question 1

def lecture(nom_fichier:str) -> list[dict]:        
    listePop = []
    with open(nom_fichier, 'r', encoding='utf-8') as objFichier:
        objDatasCsv = csv.DictReader(objFichier, delimiter=';')
        for ligne in objDatasCsv:
            listePop.append(dict(ligne))
    return listePop

dep = lecture("../src/dep59.csv")
arrondissement = lecture('../src/dep59_arrondissement.csv')

## Question 2

def attributs(liste_dico: list[dict]) -> list:
    res = []
    for cle in liste_dico[0].keys():
        res.append(cle)
    return res

## Question 4

# Code arrondissement

def jointure(table1, table2,cle1,cle2):
    table_fusion = []
    for elt_t1 in table1:
        for elt_t2 in table2:
            if elt_t1[cle1] == elt_t2[cle2]:
                new_dico = dict(elt_t1)
                for key in elt_t2:
                    if key != cle2:
                        new_dico[key] = elt_t2[key]
                table_fusion.append(new_dico)
    return table_fusion

## Question 5

def noms_communes(liste_dico: list[dict]) -> list:
    res = []
    for communes in liste_dico:
        if communes['Code arrondissement'] == '5'and communes['Population totale'] <= '5000':
            res.append(communes['Nom de la commune'])
    return res