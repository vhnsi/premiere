import csv
### Exercice 1

## Question 1

def lecture(nom_fichier:str) -> list[dict]:        
    listePop = []
    with open(nom_fichier, 'r', encoding='utf-8') as objFichier:
        objDatasCsv = csv.DictReader(objFichier, delimiter=';')
        for ligne in objDatasCsv:
            listePop.append(dict(ligne))
    return listePop

table = lecture("../src/dep59.csv")

## Question 2

def attributs(liste_dico: list[dict]) -> list:
    res = []
    for cle in liste_dico[0].keys():
        res.append(cle)
    return res

## Question 3

def nom_communes(liste_dico: list[dict]) -> list:
    res = []
    for communes in liste_dico:
        if int(communes['Population totale']) > 10000:
            res.append(communes['Nom de la commune'])
    return res