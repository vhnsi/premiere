# Traitement des données en table

## Introduction

Avec le numérique, la collecte, le traitement et l'utilisation des données est devenu omniprésent:

* cartes de fidélité;
* sites visités sur internet et comportements sur les sites;
* données médicales;
* position des moyens de transports en temps réel;
* disponibilité des vélos sur des stations de vélos partagés;
* ...

De nombreuses institutions mettent à disposition des données de manière ouverte (c'est à dire des données dont l'accés est public et libre de droit, tout comme leur exploitation. Plus d'information [ici](https://fr.wikipedia.org/wiki/Donn%C3%A9es_ouvertes).)

Exemple: [Site des données ouvertes de la MEL](https://opendata.lillemetropole.fr/pages/home/)

L'utilisation de ces données permet de rendre ou d'améliorer les services:
* temps d'attente avant le prochain bus;

mais sont aussi utilisées à des fins commerciales (publicité ciblé).

Le sigle **RGPD** signifie (Règlement Général sur la protection des Données). Le RGPD encadre le traitement des données personnelles sur le territoire de l'Union européenne. 

Plus d'information [RGPD : de quoi parle-t-on ?](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on)

## Tables de données

### Définition

Les données peuvent souvent être représentées sous forme de tableau.

> Une table est un ensemble de données organisées sous formes d'un tableau. On dit que c'est une **collection d'éléments (ou d'enregistrements)**.
> Les *lignes* du tableau sont appelées **enregistrement**. 
> Les *colonnes* représentents les **attributs** (ou **descripteurs**) d'un enregistrement.
> Chaque ligne comporte pour un enregistrement les différentes **valeurs** des **attributs**.
> Chaque attribut prend ses données dans un ensemble appelé **domaine** (ex: int, float, str...), toutes les valeurs sont de même type.

Exemple:

Le tableau suivant est une table comportant:

* 5 attributs : Marque, Modèle, Energie, Km, Année
* 3 enregistrements

|Marque|Modèle|Energie|Km|Année|
|:-:|:-:|:-:|:-:|:-:|
|VW|Polo|Essence|95600|2013|
|Renault|Zoé|Electricité|12300|2017|
|Peugeot|308|Diesel|56700|2015|


## Les fichiers csv

Les données peuvent être stockées dans des fichiers texte au format csv.

Le sigle CSV (*Comma-Separated Values* c'est à dire valeurs séparées par des virgules) est un format très pratique pour
représenter des données structurées.

Dans ce format, chaque ligne représente un enregistrement et, sur une même ligne, les différents champs de l’enregistrement sont réparés par une virgule (d’où le nom). En pratique, on peut spécifier le caractère utilisé pour séparer les différents champs et on utilise fréquemment un point-virgule, une tabulation ou deux points pour cela. 

Notons enfin que la première ligne d’un tel fichier est souvent utilisée pour indiquer le nom des différents champs.
Dans ce cas, le premier enregistrement apparaîssant en deuxième ligne du fichier.

```csv
Marque;Modèle;Energie;Km;Année
VW;Polo;Essence;95600;2013
Renault;Zoé;Electricité;12300;2017
Peugeot;308;Diesel;56700;2015
```

**Exercice**

Récupérer le fichier csv "V'Lille - Disponibilité en temps réel" (téléchargeable au format csv [ici](https://opendata.lillemetropole.fr/explore/dataset/vlille-realtime/export/?disjunctive.libelle&disjunctive.nom) depuis le site des données ouvertes de la MEL.

* Ouvrir le fichier avec un tableur (LibreOffice Calc ou excel)
* Quels sont les attributs ?
* Combien y'a-t-il de station V'Lille ?
* Quel est le nom de la station V'Lille qui contient le plus d'emplacement de vélo ?

## Importation des données d'un fichier csv en python

Documentation officielle python du module CSV [Lecture et écriture de fichiers CSV](https://docs.python.org/fr/3.7/library/csv.html)

1. Importation dans une liste de liste

Voici les étapes de l'importation:

- La première étape consiste à importer le fichier texte à l'aide de la fonction open. Le paramètre 'r' indique que le fichier est ouvert en lecture seule et le paramètre encoding='utf-8' fichier source est codé en utf-8. [Documentation de la fonction open](https://docs.python.org/3/library/functions.html#open)

- La deuxième étape consiste à transformer le texte importé en utilisant la fonction reader de la librairie csv. Cette fonction renvoie un objet qui n'est pas directement exploitable mais qui est un itérable dont chaque élément est un tableau à une dimension correspondant à une ligne du fichier csv.

- La troisième étape consiste donc à parcourir l'itérable élément par élément et à ajouter chacun de ces élément dans une liste.

```python
import csv
listePop = []
objFichier = open('population_hdf.csv', 'r', encoding='utf-8')
objDatasCsv = csv.reader(objFichier, delimiter=';')
for ligne in objDatasCsv:
    listePop.append(ligne)
objFichier.close()
print(listePop)
```

Ce code peut être simplifié en utilisant with:

```python
import csv
listePop = []
with open('population_hdf.csv', 'r', encoding='utf-8') as objFichier:
    objDatasCsv = csv.reader(objFichier, delimiter=';')
    for ligne in objDatasCsv:
        listePop.append(ligne)
```

2. Importation dans une liste de dictionnaire

La méthode est similaire.

La première étape est inchangée.

Dans la deuxième étape, on utilise la fonction DictReader (à la place de reader). Cette fonction renvoie un objet itérable dont chaque élément est un objet apparenté à un dictionnaire correspondant à une ligne du fichier csv, les clés étant les valeurs de la première ligne du fichier.

Dans la troisième étape, il faut, en plus convertir chaque élément en dictionnaire avant de l'ajouter à la liste.


```python
import csv
listePop = []
with open('population_hdf.csv', 'r', encoding='utf-8') as objFichier:
    objDatasCsv = csv.DictReader(objFichier, delimiter=';')
    for ligne in objDatasCsv:
        listePop.append(dict(ligne))
```

**Applications**

Dans cette partie, on va travailler avec le fichier csv "V'Lille - Disponibilité en temps réel" (téléchargeable au format csv [ici](https://opendata.lillemetropole.fr/explore/dataset/vlille-realtime/export/?disjunctive.libelle&disjunctive.nom) depuis le site des données ouvertes de la MEL.

**Application 1** 🟢️

Écrire une fonction `lecture(nom_fichier: str) -> list[dict]` qui prend un nom de fichier csv en paramètre , et renvoie une liste de dictionnaires correspondant aux données du fichier.

**Application 2**  🟢️

En utilisant la fonction `lecture`, écrire une fonction `nom_stations_boucle(liste_dict : list[dict]) -> list` et `nom_stations_compréhension(liste_dict : list[dict]) -> list` qui prennent en paramètre une liste de dictionnaires et renvoie la liste des noms des stations. La fonction `nom_station_boucle` utilisera une boucle et la fonction `nom_stations_compréhension` utilisera une liste en compréhension.

**Application 3**  🟢️

En utilisant la fonction `lecture`, écrire une fonction `nom_stations_hs_boucle(liste_dict : list[dict])-> list` et `nom_stations_hs_comprehension(liste_dict : list[dict]) -> list` qui prennent en paramètre une liste de dictionnaires et renvoie la liste des noms des stations qui ne sont pas 'EN SERVICE'. La fonction `nom_station_hs_boucle` utilisera une boucle et la fonction `nom_stations_hs_comprehension` utilisera une liste en compréhension.

**Application 4**  🟢️

En utilisant la fonction `lecture`, écrire une fonction `nom_station_max_vlille(liste_dict : list[dict] -> str)` qui prend en paramètre une liste de dictionnaires et renvoie le nom de la station qui contient le plus d'emplacements de V'Lille.

## Tri d'une table de données

Utilisation de la fonction `sort`

La structure de la fonction `sort` est la suivante: `liste.sort(key = fonc, reverse = False)`.

fonc est une fonction qui doit renvoyer une valeur qui servira de critère de tri.

### Exemple: tri d'une liste simple

Dans cet exemple, on souhaite trier une liste de chaînes de caractères.

Tri sans clé
```python
liste = ['clémence','bob','clémentine','alice','estelle']
liste.sort() # Le tri se fait par défaut par ordre alphabétique (pour des chaînes de caractères)

# liste contient ['alice','bob','clémence','clémentine','estelle']
```

Tri avec une clé donnée par une fonction

```python
def fonc_cle_de_tri(chaine) -> int:
	# renvoie la taille de la chaîne de caractère
	return len(chaine)
	
liste = ['clémence','bob','clémentine','alice','estelle']
liste.sort(key=fonc_cle_de_tri)
# liste contient ['bob','alice','estelle','clémence','clémentine']
```

### Exemple: tri d'une liste de listes

```python
liste = [['La ligne verte', '2000', 'Frank Darabont'],
         ['La liste de Schindler', '1994', 'Steven Spielberg'],
         ['Le voyage de Chihiro', '2002', 'Hayao Miyazaki']]

def fonc_cle_de_tri(film):
    return int(film[1])
```

### Exemple: tri d'une liste de dictionnaires


```python
liste = [{'Titre': 'La ligne verte', 'Année': '2000', 'Réalisateur': 'Frank Darabont'},
         {'Titre': 'La liste de Schindler', 'Année': '1994', 'Réalisateur': 'Steven Spielberg'},
         {'Titre': 'Le voyage de Chihiro', 'Année': '2002', 'Réalisateur': 'Hayao Miyazaki'}]

def fonc_cle_de_tri(film):
    return int(film["Année"])

liste.sort(key=fonc_cle_de_tri);

# liste contient 
# 	[{'Titre': 'La liste de Schindler', 'Année': '1994', 'Réalisateur': 'Steven Spielberg'}, 
#	{'Titre': 'La ligne verte', 'Année': '2000', 'Réalisateur': 'Frank Darabont'}, 
#	{'Titre': 'Le voyage de Chihiro', 'Année': '2002', 'Réalisateur': 'Hayao Miyazaki'}]

```

**Application 5** 🔵️

Écrire une fonction `nom_station_max_velos_dispo(nom_fichier : str) -> list` qui prend en paramètre un nom de fichier csv et renvoie la liste des noms des trois stations qui ont le plus de vélos disponibles.

## Fusion de deux tables

Lorsque l'on utilise des tables de données, il est possible d'avoir plusieurs tables différentes que l'on aimerait réunir en une seul table afin d'effectuer des opérations de filtration, de tri, etc...

Pour cela, on va utiliser une **jointure**.

La **jointure** entre deux tables permet de combiner les données selon un attribut commun.

### Exemple

On dispose de deux fichiers csv.

* Population des Haut-de-France par département. [population_hdf.csv](src/population_hdf.csv)

|dept|population|
|:-:|:-:|
|2|534490|
|59|2604361|
|60|824503|
|62|1468018|
|80|572443|

* Correspondance des numéros et des noms de départements: [departements.csv](src/departements.csv)

|code|nom|
|:-:|:-:|
|1|Ain|
|2|Aisne|
|3|Allier|
|...|...|

On souhaite obtenir une seule table. Les attributs communs sont dept et code.

|dept_code|dept_nom|population|
|:-:|:-:|:-:|
|2|Aisne|534490|
|59|Nord|2604361|
|60|Oise|824503|
|62|Pas-de-Calais|1468018|
|80|Somme|572443|

**Application 6** 

A partir des fichiers csv des départements et de la population. 

🟢️ Version Facile 🟢️

Réaliser la jointure à la main (sur papier) entre les tables [musique.csv](src/musique.csv) et [support_musique.csv](src/support_musique.csv).

🔴️ Version difficile 🔴️

Proposer un programme qui permet de réaliser la jointure les deux tables tel que présenté.

## Pour aller plus loin

### Écrire dans un fichier csv

Voici les étapes de l'écriture dans un fichier:

* La première étape consiste à importer le fichier texte à l'aide de la fonction open.

|mode| Signification|
|:----:|:---:|
|`r`| lecture (default)|
|`w`| écriture, tronque d'abord le fichier|
|`x`| création uniquement, échoue si le fichier existe déjà |
|`a`| écriture, ajoute à la fin si le fichier existe|
|`b`| mode binaire|
|`t`| mode texte (default)|
|`+`| mise à jour (lecture et écriture)|

	- le paramètre `encoding='utf-8'` permet d'indiquer que le fichier doit être codé en utf-8.

* La deuxième étape consiste, à l'aide de la fonction `writer` de la bibliothèque csv, à créer un objet spécifique dont le rôle est de convertir les données de l'utilisateur en chaine de caractères adéquat.

* La troisième étape consite à ajouter les différents enregistrements à l'aide de la fonction `writerow` de la bibliothèque csv.

**Exemple :** création d'une liste d'enregistrements sous forme de liste dont les attributs sont : Nom, Prenom et Age.

```python
import csv
with open('fichier.csv','w',encoding='utf-8') as objFichierCSV:
	objTranscipteur = csv.writer(objFichierCSV, delimiter=';')
	objTranscipteur.writerow(['Nom','Prenom','Age'])
	objTranscipteur.writerow(['Oleon','Tim','25'])
	objTranscipteur.writerow(['Calbuth','Raymond','32'])
```

**Exemple :** création d'une liste d'enregistrements sous forme de dictionnaires dont les attributs sont : Nom, Prenom et Age.

```python
import csv
with open('fichier.csv','w',encoding='utf-8') as objFichierCSV:
	liste_entetes = ['Nom','Prenom','Age']
	objTranscipteur = csv.DictWriter(objFichierCSV, fieldnammes= liste_entetes,delimiter=';')
	objTranscipteur.writeheader()
	objTranscipteur.writerow({'Nom' : 'Oleon','Prenom' : 'Tim','Age' : '25'})
	objTranscipteur.writerow({'Nom' : 'Calbuth','Prenom' : 'Raymond','Age' : '32'})
```

## Exercices

**Exercice 1** 🟢️

Dans cet exercice, on travaillera avec le fichier csv des communes du Nord [fichier csv](src/dep59.csv).

1. Écrire une fonction `csv_to_dico(nom_fichier: str) -> list(dict)` qui prend en paramètre un nom de fichier csv et renvoie les données du fichier sous forme d'une liste de dictionnaires.

2. Écrire une fonction `attributs(liste_dico : list[dict]) -> list` qui prend en paramètre une liste de dictionnaires et renvoie une liste contenant les attributs de la table de données.

3. Écrire une fonction qui affiche le nom des communes qui ont une population totale supérieur à 10 000.

**Exercice 2** 🔵️

Cet exercice s'intéresse aux pokémons. 

On dispose d'une base de données au format csv [ici](src/pokemon.csv) de l'ensemble des pokémons et de leurs caractéristiques.

1. Écrire une fonction qui permet d'extraire les données du fichier sous la forme d'une liste de dictionnaires.

2. Écrire une fonction qui renvoie la liste des attributs du fichier de données.

3. Écrire une fonction qui renvoie la liste des noms des pokémons dont l'attributs `Attack` est supérieur à une valeur passée en paramètre. 

4. Écrire une fonction qui affiche les noms des pokémons qui apparaissent plusieurs fois.

5. Écrire une fonction qui calcule la moyenne des attaques (attribut `Attack`) des pokémons dont les points de vie (attribut `HP`) sont inférieurs à une valeur passé en paramètre. 

**Exercice 3** 🔵️

Traitement avec un tableur

* Aller sur [https://www.data.gouv.fr/fr/](https://www.data.gouv.fr/fr/).
* Trouver les données "Communes de France - Base des codes postaux" fournies par Mohamed Badaoui et télécharger le fichier *communes-departements-regions.csv*.

* Ouvrir le fichier un notepad++

1. Quel est le séparateur du fichier csv ?
2. Quels sont les attributs ?

* Ouvrir le fichier avec LibreOffice Calc

3. Quel est le nombre de communes en France d'après ce fichier ?
4. Quelles sont les coordonnées GPS de la ville d'Haubourdin ?
5. Quel est le nombre de communes dans le département 59 ?
6. Quelle est la ville de plus haute latitude ? de plus basse longitude ?
7. Combien de communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M ?
8. Quelles sont les 5 communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M *et* qui ont la plus grande latitude ?

**Exercice 4** 🔵️

Reprendre les questions 3 à 8 de l'exercice 3 avec python.

**Exercice 5** 🔵️

Dans cet exercice, on travaillera avec le fichier csv des communes du Nord [fichier communes csv](src/dep59.csv) et celui des arrondissements [fichier arrondissements csv](src/dep59_arrondissement.csv).

**À partir du fichier arrondissements:**

1. Écrire une fonction `csv_to_dico(nom_fichier: str) -> list[dict]` qui prend en paramètre un nom de fichier csv et renvoie les données du fichier sous forme d'une liste de dictionnaires.

2. Écrire une fonction `attributs(liste_dico : list[dict]) -> list` qui prend en paramètre une liste de dictionnaires et renvoie une liste contenant les attributs de la table de données.

3. Quels sont les attributs communs des deux tables ?

🔴️ Difficile 🔴️

On souhaite effectuer une jointure entre ces deux tables afin d'associer les communes a leurs arrondissements. Selon-vous qu'elle serait l'attribut à joindre.

4. Écrire un programme qui réalise la jointure entre les deux tables.

5. Écrire une fonction qui affiche les noms des communes de l'arrondissement 5 et qui a une population totale inférieur à 5 000.


