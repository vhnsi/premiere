# Lire et écrire dans un fichier texte avec python

## Ouverture

Le caractère de saut de ligne est `\n`.
	
Voici les étapes de l'ouverture et d'écriture d'un fichier:

* La première étape consiste à importer le fichier texte à l'aide de la fonction open.

|mode| Signification|
|:----:|:---:|
|`r`| lecture (default)|
|`w`| écriture, tronque d'abord le fichier|
|`x`| création uniquement, échoue si le fichier existe déjà |
|`a`| écriture, ajoute à la fin si le fichier existe|
|`b`| mode binaire|
|`t`| mode texte (default)|
|`+`| mise à jour (lecture et écriture)|

	- le paramètre `encoding='utf-8'` permet d'indiquer que le fichier doit être codé en utf-8.

```python
file = open('nom_fichier', 'mode', encoding='utf-8')
traitement ...
```

Avec cette première méthode il faut impérativement fermer le fichier manuellement avec la fonction `close`

```python
file.close()
```

La seconde méthode permet de se passer de la fonction close, néanmoins le fichier n'est accessible uniquement lors du with.

```python
with open('nom_fichier.csv', 'mode', encoding='utf-8') as file:
	traitement ...
```

## Lecture

* La méthode `read` permet de lire le fichier en sont intégralatité.
```python
file = open('test.txt','r')
res = file.read()
file.close()
```
La variable res contient tout le texte du fichier par exemple `'Un test bien sympathique \nBesoins de \n`.

* La méthode `readline` permet de lire le fichier ligne par ligne.

```python
file = open('test.txt','r')
ligne1 = file.readline()
# ligne1 contient 'Un test bien sympathique \n'
ligne2 = file.readline()
# ligne2 contient 'Besoins de \n'
ligne3 = file.readline()
# ligne3 contient '' 
file.close() 
```

Dès lors que l'on obtient une chaîne vide, cela signifie que l'on est à la fin du fichier.

* La méthode `readlines` permet de lire l'intégralité du fichier et de stocker chaque ligne dans une liste.

```python
file = open('test.txt','r')
lignes = file.readlines()
# lignes contient ['Un test bien sympathique \n','Besoins de \n']
file.close()
```

## Écriture

On utilisera le mode écriture : `w` (pour écrire), `a` (pour ajouter).

```python
file = open("test2.txt", "w")
file.write("un petit texte\nsur deux lignes\n")
file.close()
```
